## Laravel + Vue.js Employee Adminpanel Demo

---

### How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate__
- Run __php artisan passport:install__
- Run __npm install__
- Run __npm run dev__
- Credentials __admin@admin.com__ / __password__.

---

### To do
- fix height of sidebar
- e.g, put in sass file 

 section.sidebar {
    padding-bottom: 10px;
    height: 100vh!important;
}
